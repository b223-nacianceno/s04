<?php require_once "./code.php" ?>
<!DOCTYPE html>
	<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>S04: Access Modifiers</title>
	</head>
	<body>

		<h1>Access Modifiers</h1>

		<h2>Building Object Variable</h2>
		<p><?php var_dump($building)?></p>
		<p><?php //echo $building->name;?></p>

		<h2>Condominium Object Variable</h2>
		<p><?php var_dump($condominium)?></p>
		<p><?php //echo $condominium->name;?></p>

		<h1>Encapsulation</h1>
		<p>The name of the condominium is <?php echo $condominium->getName(); ?></p>
		<?php $condominium->setName('Enzo Tower'); ?>
		<?php $condominium->setName(''); ?>
		<p>The name of the condominium has been changed to <?php echo $condominium->getName(); ?></p>

		<p><?php var_dump($condominium)?></p>

	</body>
</html>
