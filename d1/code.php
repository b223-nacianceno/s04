<?php

class Building{

	// Access modifiers
		// This are keywords that can be used to control the visibility of fields/properties, methods and constructor in a class.

		// public: fully open.

		// private: cannot be directly accessed and inherited.
			// The private modifier also disables inheritance of its properties and methods to child classes.

		// protected: cannot be directly accessess but it can be inherited.
			// The protected modifier will allow inheritance of properties and methods to child classes.

	public $name;
	public $floors;
	public $address;

	public function __construct($name, $floors, $address){
		$this->name = $name;
		$this->floors = $floors;
		$this->address = $address;
	}

}


$building = new Building('Caswynn Building', 8, 'Timog Avenue, Quezon City, Philippines');


class Condominium extends Building{
	// Encapsulation indicates that data must not be directly accessible to users but through a public function (setter and getter).

	// These functions serve as the intermediary in access the object's property.

	// getter (accessors)
	public function getName(){
		return $this->name;
	}

	// setter (mutators)
	public function setName($name){
		$this->name = $name;

		// We can add validation for changing a specific property.
		// if(strlen($name) !== 0 ){
		// 	$this->name = $name;
		// }
	}
}

$condominium = new Condominium('Enzo Condo', 5, 'Buendia Avenue, Makati City, Philippines');
