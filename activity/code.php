<?php

class Building{

	private $name;
	private $floors;
	private $address;

	public function __construct($name, $floors, $address){
		$this->name = $name;
		$this->floors = $floors;
		$this->address = $address;
	}
    // Getter (Name)
    public function getName(){
		return $this->name;
	}
    // Setter (Name)
	public function setName($name){
		$this->name = $name;
	}
    // Getter (Floors)
    public function getFloors(){
		return $this->floors;
	}
    // Setter (Floors)
	public function setFloors($floors){
		$this->floors = $floors;
	}
    // Getter (Address)
    public function getAddress(){
		return $this->address;
	}
    // Setter (Address)
	public function setAddress($address){
		$this->address = $address;
	}

}


$building = new Building('Caswynn Building', 8, 'Timog Avenue, Quezon City, Philippines');


class Condominium extends Building{

	// public function getName(){
	// 	return $this->name;
	// }

	// public function setName($name){
	// 	$this->name = $name;

	// }
}

$condominium = new Condominium('Enzo Condo', 5, 'Buendia Avenue, Makati City, Philippines');
