<?php require_once "./code.php" ?>
<!DOCTYPE html>
	<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>S04: Access Modifiers</title>
	</head>
	<body>

		<h1>Access Modifiers</h1>

		<h2>Building</h2>
        <p>The name of the building is <?php echo $building->getName(); ?>.</p>
        <p>The <?php echo $building->getName(); ?> has <?php echo $building->getFloors(); ?> floors.  </p>
        <p>The <?php echo $building->getName(); ?> is located at <?php echo $building->getAddress(); ?>. </p>

        <p> <?php echo $building->setName('Caswynn Complex'); ?> </p>
        <p>The name of the building has been changed to <?php echo $building->getName(); ?>.</p>

		<h2>Condominium </h2>
		<p>The name of the condominium is <?php echo $condominium->getName(); ?></p>
		<?php $condominium->setName('Enzo Condo'); ?>

        <p>The <?php echo $condominium->getName(); ?> has <?php echo $building->getFloors(); ?> floors. </p>

        <p>The <?php echo $condominium->getName(); ?> is located at <?php echo $condominium->getAddress(); ?>. </p>

        <p> <?php echo $building->setName('Enzo Tower'); ?> </p>
        <p>The name of the building has been changed to <?php echo $condominium->getName(); ?>.</p>

	</body>
</html>
